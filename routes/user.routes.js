const userController = require("../controllers/user.controller");
const express = require("express");
const userRouter = express.Router();

userRouter.get("/", userController.getUsers);
userRouter.post("/", userController.createUser);

module.exports = userRouter;
