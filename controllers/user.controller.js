const db = require("../models");
const { user } = db;
// get users
exports.getUsers = async (req, res) => {
  try {
    const users = await user.findAll();
    res.json(users);
  } catch (err) {
    res.status(500).json({
      message: err.message,
    });
  }
};

exports.createUser = async (req, res) => {
  try {
    const user = await db.user.create(req.body);
    res.json(user);
  } catch (err) {
    res.status(500).json({
      message: err.message,
    });
  }
};
